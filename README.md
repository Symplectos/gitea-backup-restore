# Back up and Restore Gitea Instances
The two bash scripts in this repository can be used to back up or restore a [Gitea](https://gitea.io/) instance. 

This repository is managed on [Codeberg](https://codeberg.org).

The scripts must be configured by defining a few variables in the **VARIABLES** sections.

**Supported Databases**:
 * PostgreSQL 12

## PostgreSQL
The script assumes a **.pgpass** file to exist in the root home folder, with at least the following information for the database to be backed up, and the user to connect to the database to:

```
hostname:port:database:username:password
localhost:5432:gitea:gitea:***
```

**Note**: The file must have **0600** permissions.

**Warning**: The script assumes that the database was created with **UTF-8** support, and **en_IE** locales.

## Further Configuration
The following variables should be set to adapt the script to the environment in question.
| Variable | Description | Default |
| :------- | :---------- | :------ |
| backupRoot | the root directory for the backups | /mnt/backup/gitea |
| databaseHost | the name of the server hosting the database | localhost |
| databasePort | the port the database listens on | 5432 |
| giteaDB | the name of the Gitea database  | gitea |
| giteaDBUser | the user to access the Gitea database | gitea |
| giteaUser | the system user who runs the Gitea instance | gitea |
| giteaApplicationFile | the location of the app.ini file | /etc/gitea/app.ini |
| giteaWorkingDirectory | the Gitea working directory | /home/gitea |
| giteaCustomDirectory | the Gitea custom directory | /home/gitea/custom |
| giteaDataDirectory | the Gitea data directory (restore only) | /home/gitea/custom |
| giteaLogDirectory | the Gitea log directory (restore only) | /home/gitea/custom |
| giteaRepositoriesDirectory | the Gitea repository directory (restore only) | /home/gitea/custom |
| useCompression | true iff compression should be used | false |
| webServerService | the name of the web or proxy server | nginx |
| nBackupsToKeep | the number of backups to keep | 7 |
| giteaBackupFile | the name of the backup file | gitea.tar |
| giteaBackupFileType | output format for the backup file | tar |
| giteaDBDumpFile | the name of the backup file for the database dump | gitea-db.dump |

**NB 1**: If **useCompression** is set to **true**, **.gz** will be appended to the file name and file type.

**NB 2**: If another backup software, such as Borg, is to be used, compression should be disabled.

## Automation with Cronjobs
I suggest running the backup script each evening and keeping 7 backups. To do so, a cronjob must be created for the root user.

```
sudo crontab -e
```

```
0 5 * * * /bin/bash '/home/symplectos/Scripts/Backup/Gitea/giteaBackup.sh'
```

**Note**: Make sure the script is executable.

## Manual Usage

### Backup
Simply run the backup script with superuser do:

```
sudo ./giteaBackup.sh
```

### Restore
To restore a backup, run the script, with superuser do, specifying the desired backup to restore as parameter:

```
sudo ./giteaRestore.sh 20210325_050000
```

## Test Run

```
sudo ./giteaBackup.sh

Creating a backup of the Gitea instance ...

Backup Directory: /mnt/backup/gitea
Backup Date: 20210331_153856


Stopping the web server ... done
Shutting down Gitea ... done

Dumping Gitea data ... done
Dumping the Gitea DB (PostgreSQL) ... done

Restarting Gitea ... done
Restarting the web server ... done


The Backup of the Gitea instance was successful!
Created Backup: /mnt/backup/gitea/20210331_153856/
This Backup can now be assimilated by the Borg!
```

To restore this backup, **20210331_153856** should be given as a parameter to the restore script:

```
sudo ./giteaRestore.sh 20210331_153856

Restoring Backup: /mnt/backup/gitea/20210331_153856

Stopping the web server ... done
Shutting down Gitea ... done

Deleting the old Gitea instance ... done
Restoring the Gitea directories ... done

Dropping the old Gitea DB ... done
Creating the new Gitea DB ... done
Importing the DB dump into the new DB ... done

Starting the web server ... done
Restarting Gitea ... done
Updating the git-hooks ... done


Done: The Backup 20210331_153856 was successfully restored!
```

# References
* [Gitea](https://docs.gitea.io/en-us/backup-and-restore/)