#!/bin/bash

########################################################################################################################
# Bash script to create backups of a Gitea instance
#
# Author: Gilles Bellot
# Date: 31/03/2021
# Location: Lenningen, Luxembourg
#
# Notes:
#   - currently only supports PostgreSQL databases
#   - if a backup tool like Borg is used, compression should be set to false
#   - a separate db dump is created
#
# References:
#  - Gitea: https://docs.gitea.io/en-us/backup-and-restore/
########################################################################################################################

########################################################################################################################
# VARIABLES ############################################################################################################
########################################################################################################################

# create backup directories under the backupRoot with a timestamp
backupRoot='/mnt/backup/gitea'
currentDate=$(date +"%Y%m%d_%H%M%S")
backupDirectory="${backupRoot}/${currentDate}/"

# specify the database host
databaseHost='localhost'

# specify the port the database is listening on
databasePort=5432

# specify the gitea database
giteaDB='gitea'

# specify the user to access the gitea database
giteaDBUser='gitea'

# specify the gitea user
giteaUser='gitea'

# specify the location of the gitea application ini file
giteaApplicationFile='/etc/gitea/app.ini'

# specify the location of the gitea working directory
giteaWorkingDirectory='/home/gitea'

# specify the location of the gitea custom directory
giteaCustomDirectory='/home/gitea/custom'

# define whether to use compression or not (see notes above)
useCompression=false

# specify the name of the web server or reverse proxy user
webServerService='nginx'

# define whether the web service should be stopped and restarted
stopWebServer=true

# define the number of backups to keep, if set to 0, all backups are kept
nBackupsToKeep=7

# define file names for the backups
giteaBackupFile='gitea.tar'
giteaBackupFileType='tar'

if [ "$useCompression" = true ]; then
  giteaBackupFile="${giteaBackupFile}.gz"
  giteaBackupFileType="${giteaBackupFileType}.gz"
fi

giteaDBDumpFile='gitea-db.dump'

########################################################################################################################
# METHODS ##############################################################################################################
########################################################################################################################

# print error messages
errorecho() { cat <<<"$@" 1>&2; }

# capture CTRL+C input and optionally restart the nextcloud instance before quitting
trap CtrlC INT

function CtrlC() {
  # ask user for confirmation
  echo "Cancelling Backup ..."
  echo
  if [ "$stopWebServer" = true ]; then
    echo "Restarting the web server ..."
    systemctl start "${webServerService}"
    echo "Done"
  fi
  echo

  exit 1
}

########################################################################################################################
# SCRIPT ###############################################################################################################
########################################################################################################################

# print starting info
echo
echo "Creating a backup of the Gitea instance ..."
echo
echo "Backup Directory: ${backupRoot}"
echo "Backup Date: ${currentDate}"
echo
echo

# make sure the script is run as root user
if [ "$(id -u)" != "0" ]; then
  errorecho "Critical Error: The backup script has to be run as root!"
  exit 1
fi

# check for existing directory
if [ ! -d "${backupDirectory}" ]; then
  mkdir -p "${backupDirectory}"
else
  errorecho "Critical Error: The backup directory ${backupDirectory} already exists!"
  exit 1
fi

# stop the web server (reverse proxy)
if [ "$stopWebServer" = true ]; then
  echo -n "Stopping the web server ..."
  systemctl stop "${webServerService}"
  echo " done"
fi

# stop gitea
echo
echo -n "Shutting down Gitea ..."
systemctl stop gitea.service
echo " done"

# create the data dump
echo -n "Dumping Gitea data ..."
tmpBackupFile="${giteaWorkingDirectory}/${giteaBackupFile}"
sudo -u ${giteaUser} gitea dump --config ${giteaApplicationFile} --custom-path ${giteaCustomDirectory}  --work-path ${giteaWorkingDirectory} --type ${giteaBackupFileType} --file ${tmpBackupFile} &>/dev/null
mv ${tmpBackupFile} "${backupDirectory}/${giteaBackupFile}"
echo " done"

# backup the Gitea DB
echo -n "Dumping the Gitea DB (PostgreSQL) ..."
if ! [ -x "$(command -v pg_dump)" ]; then
  errorecho "Critical Error: PostgreSQL not installed (command pg_dump not found)!"
  errorecho "Critical Error: Unable to dump the Gitea DB!"
else
  # dump the db, as tar file with the ability to create the database from scratch
  pg_dump --host="${databaseHost}" --port="${databasePort}" --dbname="${giteaDB}" --username="${giteaDBUser}" --no-password --file="${backupDirectory}/${giteaDBDumpFile}" --format=tar --create
fi
echo " done"

# restart gitea
echo
echo -n "Restarting Gitea ..."
systemctl start gitea.service
echo " done"

# restart the web server
if [ "$stopWebServer" = true ]; then
  echo -n "Restarting the web server ..."
  systemctl start "${webServerService}"
  echo " done"
  echo
fi

# delete old backups
if ((nBackupsToKeep != 0)); then
  # get number of backup directories
  nBackups=$(find $backupRoot -mindepth 1 -maxdepth 1 -type d | wc -l)

  if ((nBackups > nBackupsToKeep)); then
    echo "Removing old backups ..."
    echo

    # remove old backup directories
    find $backupRoot -mindepth 1 -maxdepth 1 -type d -printf '%T@\t%p\0' | sort --zero-terminated --numeric-sort --reverse | tail --zero-terminated --lines=$((nBackups - nBackupsToKeep >= 0 ? nBackups - nBackupsToKeep : 0)) | xargs --null --no-run-if-empty --max-args=1 echo | cut --fields=2 | xargs --no-run-if-empty rm -rf --verbose
  fi
fi

echo
echo "The Backup of the Gitea instance was successful!"
echo
echo "Created Backup: ${backupDirectory}"
if [ "$useCompression" = false ]; then
  echo "This Backup can now be assimilated by the Borg!"
fi