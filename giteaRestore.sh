#!/bin/bash

########################################################################################################################
# Bash script to restore a Gitea instance from a Backup
#
# Author: Gilles Bellot
# Date: 31/03/2021
# Location: Lenningen, Luxembourg
#
# Notes:
#   - currently only supports PostgreSQL databases
#   - public keys must be recreated (at least one)
#   - if a backup tool like Borg is used, compression should be set to false
#
# Usage:
#   - ./giteaRestore.sh <backup> (i.e. ./giteaRestore.sh 20210315_050000)
#
# References:
#  - Gitea: https://docs.gitea.io/en-us/backup-and-restore/
########################################################################################################################

########################################################################################################################
# VARIABLES ############################################################################################################
########################################################################################################################

# get backup to restore (by date)
restore=$1
backupRoot='/mnt/backup/gitea'
restorationDirectory="${backupRoot}/${restore}"

# specify the gitea user
giteaUser='gitea'

# specify the location of the gitea application ini file
giteaApplicationFile='/etc/gitea/app.ini'

# specify the location of the gitea working directory
giteaWorkingDirectory='/home/gitea'

# specify the location of the gitea custom directory
giteaCustomDirectory='/home/gitea/custom'

# specify the location of the gitea data directory
giteaDataDirectory='/home/gitea/data'

# specify the location of the gitea log directory
giteaLogDirectory='/home/gitea/log'

# specify the location of the gitea repositories directory
giteaRepositoriesDirectory='/home/gitea/gitea-repositories'

# define whether to use compression or not (see notes above)
useCompression=false

# specify the name of the web server or reverse proxy user
webServerService='nginx'

# define whether the web service should be stopped and restarted
stopWebServer=true

# specify the database host
databaseHost='localhost'

# specify the port the database is listening on
databasePort=5432

# specify the name of the nextcloud database
giteaDB='gitea'

# specify the user to access the nextcloud database
giteaDBUser='gitea'

# define file names for the backups
giteaBackupFile='gitea.tar'
giteaBackupFileType='tar'

if [ "$useCompression" = true ]; then
  giteaBackupFile="${giteaBackupFile}.gz"
  giteaBackupFileType="${giteaBackupFileType}.gz"
fi

giteaDBDumpFile='gitea-db.dump'

########################################################################################################################
# CHECKS ###############################################################################################################
########################################################################################################################

# print error messages
errorecho() { cat <<< "$@" 1>&2; }

# check for valid parameters
if [ $# != "1" ]
then
    errorecho "Critical Error: Please specify the Backup to restore!"
    errorecho "Usage: ./giteaRestore.sh 'BackupDate'"
    exit 1
fi

# make sure the script is run as root user
if [ "$(id -u)" != "0" ]
then
    errorecho "Critical Error: The backup script has to be run as root!"
    exit 1
fi

# check for valid directory
if [ ! -d "${restorationDirectory}" ]
then
    errorecho "Critical Error: Backup ${restore} not found!"
    exit 1
fi

# check for DB installation
if ! [ -x "$(command -v psql)" ]; then
  errorecho "Critical Error: PostgreSQL not installed (command psql not found)."
  errorecho "Critical Error: Unable to restore DB!"
  errorecho "The restoration was cancelled!"
  exit 1
fi

########################################################################################################################
# SCRIPT ###############################################################################################################
########################################################################################################################

# echo starting message
echo
echo "Restoring Backup: $restorationDirectory"
echo

# stop the web server (reverse proxy)
if [ "$stopWebServer" = true ]; then
  echo -n "Stopping the web server ..."
  systemctl stop "${webServerService}"
  echo " done"
fi

# stop gitea

echo -n "Shutting down Gitea ..."
systemctl stop gitea.service
echo " done"
echo

# delete old gitea directories
echo -n "Deleting the old Gitea instance ..."
rm "${giteaApplicationFile}"
rm -r "${giteaCustomDirectory}" "${giteaLogDirectory}" "${giteaDataDirectory}" "${giteaRepositoriesDirectory}" "${giteaWorkingDirectory}"
mkdir -p "${giteaWorkingDirectory}" "${giteaCustomDirectory}" "${giteaDataDirectory}" "${giteaRepositoriesDirectory}" "${giteaLogDirectory}"
echo " done"

# restore installation directory
echo -n "Restoring the Gitea directories ..."

# (unzip) and tar
tmpDir="${restorationDirectory}/tmp"
mkdir -p "${tmpDir}"
if [ "$useCompression" = true ] ; then
    tar --extract --touch --preserve-permissions --gunzip --file="${restorationDirectory}/${giteaBackupFile}" --directory="${tmpDir}"
else
    tar --extract --touch --preserve-permissions --file="${restorationDirectory}/${giteaBackupFile}" --directory="${tmpDir}"
fi

# move to correct directories
mv "${tmpDir}/app.ini" "${giteaApplicationFile}"
mv "${tmpDir}/custom/"* "${giteaCustomDirectory}/"
mv "${tmpDir}/data/"* "${giteaDataDirectory}/"
mv "${tmpDir}/log/"* "${giteaLogDirectory}/"
mv "${tmpDir}/repos/"* "${giteaRepositoriesDirectory}/"

# set ownership and permissions
chown -R ${giteaUser}:${giteaUser} ${giteaWorkingDirectory} ${giteaCustomDirectory} ${giteaDataDirectory} ${giteaRepositoriesDirectory} ${giteaLogDirectory}
chown -R root:${giteaUser} ${giteaApplicationFile}
chmod 750 -R ${giteaWorkingDirectory} ${giteaCustomDirectory} ${giteaDataDirectory} ${giteaRepositoriesDirectory} ${giteaLogDirectory}
chmod 640 -R ${giteaApplicationFile}
echo " done"
echo

# restore the DB
echo -n "Dropping the old Gitea DB ..."
sudo -u postgres psql --command="DROP DATABASE ${giteaDB};" 1>/dev/null
echo " done"

echo -n "Creating the new Gitea DB ..."
sudo -u postgres psql --command="CREATE DATABASE ${giteaDB} WITH OWNER ${giteaDBUser} TEMPLATE template0 ENCODING \"UTF8\" LC_COLLATE \"en_IE.UTF-8\" LC_CTYPE \"en_IE.UTF-8\";" 1>/dev/null
echo " done"

echo -n "Importing the DB dump into the new DB ..."
sudo pg_restore --host=${databaseHost} --port=${databasePort} --dbname="${giteaDB}" --username="${giteaDBUser}" --no-password "${restorationDirectory}"/"${giteaDBDumpFile}" #--create --clean --if-exist
echo " done"
echo

# start the web server (reverse proxy)
if [ "$stopWebServer" = true ]; then
  echo -n "Starting the web server ..."
  systemctl start "${webServerService}"
  echo " done"
fi

# remove tmp files
rm -rf "${tmpDir}"

# restart gitea
echo -n "Restarting Gitea ..."
systemctl start gitea.service
echo " done"
echo

# update the application hooks
echo -n "Updating the git-hooks ..."
sudo -u ${giteaUser} gitea admin regenerate hooks --config ${giteaApplicationFile} --custom-path ${giteaCustomDirectory}  --work-path ${giteaWorkingDirectory} &>/dev/null
echo " done"
echo

echo
echo "Done: The Backup ${restore} was successfully restored!"